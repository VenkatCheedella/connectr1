package com.example.venkat.connectr1;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

/**
 * Created by venkat on 7/10/2015.
 */
public class Adapter_NavigationDrawerList extends RecyclerView.Adapter<Adapter_NavigationDrawerList.ViewHolder>{

    private List<Map<String,?>> nav_draw_items;
    private Context context;
    private OnItemClickListner navItemClickListner;
    int currentPosition;

    public Adapter_NavigationDrawerList(List<Map<String, ?>> nav_items, Context context) {
        nav_draw_items = nav_items;
        this.context = context;
    }

    public Object getItem(int position){ return nav_draw_items.get(position);}

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView item_img;
        public TextView item_text;
        public ImageView img_divider;
        public TextView view_text;
        View mView;
        int currviewType;

        public ViewHolder(View view, int viewType) {
            super(view);
            mView = view;
            currviewType = viewType;
            item_img = (ImageView)view.findViewById(R.id.taskimg);
            item_text = (TextView)view.findViewById(R.id.tasktext);
            img_divider = (ImageView)view.findViewById(R.id.nav_div);
            view_text = (TextView)view.findViewById(R.id.view_text);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(navItemClickListner != null){
                        navItemClickListner.OnItemClick(v, getPosition());
                    }
                    currentPosition = getPosition();
                    notifyDataSetChanged();
                }
            });
        }

        public void bindData(Map<String,?> nav_item, int position){
            if(currentPosition == position)
            {
                mView.setBackgroundColor(Color.parseColor("#3285F0"));
//                if(currviewType == 1)
//                    item_text.setTextColor(Color.parseColor("#FFFFFF"));
//                if(currviewType == 3)
//                    view_text.setTextColor(Color.parseColor("#FFFFFF"));


            }
            else
            {
                mView.setBackgroundColor(0x00000000);
            }

            switch (currviewType){
                case 1:
                    if(item_img != null){
                        item_img.setImageResource( ((Integer)nav_item.get("itemimage")).intValue());
                    }
                    if(item_text != null){
                        item_text.setText((String)nav_item.get("itemname"));
                    }
                    break;
                case 2:
                    if(img_divider != null){
                        img_divider.setImageResource(((Integer)nav_item.get("itemimage")).intValue());
                    }
                    break;
                case 3:
                    if(view_text != null){
                        view_text.setText((String)nav_item.get("itemname"));
                    }
                    break;
                default:
                    break;
            }

        }
    }

    @Override
    public Adapter_NavigationDrawerList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View _view;
        switch(viewType){
            case 1:
                _view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_navigation_draw1, parent, false);
                break;
            case 2:
                _view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_navigation_draw2,parent, false);
                break;
            case 3:
                _view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_navigation_draw3, parent, false);
                break;
            default:
                _view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_navigation_draw1, parent, false);

        }
        ViewHolder nav_vh = new ViewHolder(_view, viewType);
        return nav_vh;
    }

    @Override
    public void onBindViewHolder(Adapter_NavigationDrawerList.ViewHolder holder, int position) {
        Map<String,?> nav_item = nav_draw_items.get(position);
        holder.bindData(nav_item, position);
    }

    @Override
    public int getItemViewType(int position) {
        Map<String,?> currItem = nav_draw_items.get(position);
        return (Integer)currItem.get("type");
    }

    @Override
    public int getItemCount() {
        return nav_draw_items.size();
    }

    public interface OnItemClickListner{
         void OnItemClick(View view, int position);
    }

    public void setOnItemClickListner(final OnItemClickListner navItemClickListner){
            this.navItemClickListner = navItemClickListner;
    }
}
