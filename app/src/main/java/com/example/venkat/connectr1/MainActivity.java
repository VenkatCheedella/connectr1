package com.example.venkat.connectr1;

import android.content.res.Configuration;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.HashMap;
//import android.widget.Toolbar;


public class MainActivity extends ActionBarActivity
                                  implements Fragment_Electronics.OnListItemSelectedListener{

    Toolbar mToolbar;
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToggle;
    RecyclerView myNavList;
    Adapter_NavigationDrawerList navAdapter;
    RelativeLayout nav_DrawSelection;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.mainactivity_container, new Fragment_Default())
                .commit();

        nav_DrawSelection = (RelativeLayout)findViewById(R.id.navigation_drawer);
        mToolbar = (Toolbar)findViewById(R.id.toolbar_mactivity);
        setSupportActionBar(mToolbar);

        myNavList = (RecyclerView)findViewById(R.id.navdraw_recycler);
        myNavList.setLayoutManager(new LinearLayoutManager(this));

        Data_NavgationDrawer navdata = new Data_NavgationDrawer();
        navdata.loadNavData();
        navAdapter = new Adapter_NavigationDrawerList(navdata.getNavItems(), this);

        navAdapter.setOnItemClickListner(new Adapter_NavigationDrawerList.OnItemClickListner() {
            @Override
            public void OnItemClick(View view, int position) {
                selectedView(position);
            }
        });
        myNavList.setAdapter(navAdapter);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.layout_drawer);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.Open_Drawer, R.string.Close_Drawer);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    private void selectedView(int position){
        switch (position){
            case 0:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.mainactivity_container, new Fragment_Default())
                        .commit();
                break;
            case 1:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.mainactivity_container, Fragment_Electronics.newInstance())
                        .commit();
                break;
            default:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.mainactivity_container, new Fragment_Default())
                        .commit();
                break;
        }

        mDrawerLayout.closeDrawer(nav_DrawSelection);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void OnListItemSelected(int position, HashMap<String, ?> movie) {
        Log.d("ListView Item", "is being selected");
    }
}
