package com.example.venkat.connectr1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by venkat on 7/10/2015.
 */
public class Data_NavgationDrawer {
    private List<Map<String,?>> nav_draw_items;

    public Data_NavgationDrawer() {
        this.nav_draw_items = new ArrayList<Map<String,?>>();
    }

    public List<Map<String,?>> getNavItems(){
        return nav_draw_items;
    }

    public void loadNavData(){
        HashMap<String, ?> item1 = (HashMap<String,?>)createList("Task2", R.mipmap.taskimg, 1);
        HashMap<String, ?> item2 = (HashMap<String,?>)createList("List View", R.mipmap.list, 1);
        HashMap<String, ?> item3 = (HashMap<String,?>)createList("Grid View", R.mipmap.grid, 1);
        HashMap<String, ?> item4 = (HashMap<String,?>)createList("", R.mipmap.blue_div, 2);
        HashMap<String, ?> item5 = (HashMap<String,?>)createList("View1", 0, 3);
        HashMap<String, ?> item6 = (HashMap<String,?>) createList("View2", 0, 3);
        HashMap<String, ?> item7 = (HashMap<String,?>)createList("About me", 0, 3);
        nav_draw_items.add(item1);
        nav_draw_items.add(item2);
        nav_draw_items.add(item3);
        nav_draw_items.add(item4);
        nav_draw_items.add(item5);
        nav_draw_items.add(item6);
        nav_draw_items.add(item7);

    }

    private static HashMap createList(String _itemname, int _itemImage, int _type ){
        HashMap  nav_item = new HashMap<>();
        nav_item.put("itemname", _itemname);
        nav_item.put("itemimage", _itemImage);
        nav_item.put("type", _type);
        return nav_item;
    }
}
